package rs.projekatOSA2019_maven.entity;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import rs.projekatOSA2019_maven.dto.AccountDTO;

public class JwtAuthenticationToken extends UsernamePasswordAuthenticationToken{

	private String token;
	
	public JwtAuthenticationToken(String token) {
		super(null, null);
		this.token=token;
		// TODO Auto-generated constructor stub
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	 @Override
	    public Object getCredentials() {
	        return null;
	    }

	    @Override
	    public Object getPrincipal() {
	    	AccountDTO a=new AccountDTO();
	        return a;
	}

}
