package rs.projekatOSA2019_maven.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import rs.projekatOSA2019_maven.dto.AccountDTO;
import rs.projekatOSA2019_maven.security_security.JwtGenerator;


@RestController
@RequestMapping("/token")
public class TokenController {
	
	 private JwtGenerator jwtGenerator;

	    public TokenController(JwtGenerator jwtGenerator) {
	        this.jwtGenerator = jwtGenerator;
	    }

	    @PostMapping
	    public String generate(@RequestBody final AccountDTO jwtUser) {

	        return jwtGenerator.generate(jwtUser);

	}

}
