package rs.projekatOSA2019_maven.security_config;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import rs.projekatOSA2019_maven.security_security.JwtAuthenticationEntryPoint;
import rs.projekatOSA2019_maven.security_security.JwtAuthenticationProvider;
import rs.projekatOSA2019_maven.security_security.JwtAuthenticationTokenFilter;
import rs.projekatOSA2019_maven.security_security.JwtSuccessHandler;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	   @Autowired
	    private JwtAuthenticationProvider authenticationProvider;
	    @Autowired
	    private JwtAuthenticationEntryPoint entryPoint;

	    @Bean
	    public AuthenticationManager authenticationManager() {
	        return new ProviderManager(Collections.singletonList(authenticationProvider));
	    }

	    @Bean
	    public JwtAuthenticationTokenFilter authenticationTokenFilter() {
	        JwtAuthenticationTokenFilter filter = new JwtAuthenticationTokenFilter();
	        filter.setAuthenticationManager(authenticationManager());
	        filter.setAuthenticationSuccessHandler(new JwtSuccessHandler());
	        return filter;
	}

//    @Override
//    protected void configure(HttpSecurity security) throws Exception
//    {
//    	security.csrf().disable(); //za put i post
//    	security.authorizeRequests().antMatchers("/").permitAll();
//    	security.httpBasic().disable();
//    }
	    
	    @Override
	    protected void configure(HttpSecurity http) throws Exception {

	        http.csrf().disable()
	                .authorizeRequests().antMatchers("**/rest/**").authenticated()
	                .and()
	                .exceptionHandling().authenticationEntryPoint(entryPoint)
	                .and()
	                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

	        http.addFilterBefore(authenticationTokenFilter(), UsernamePasswordAuthenticationFilter.class);
	        http.headers().cacheControl();

	}
}