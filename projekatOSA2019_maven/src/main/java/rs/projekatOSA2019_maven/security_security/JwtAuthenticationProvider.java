package rs.projekatOSA2019_maven.security_security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import rs.projekatOSA2019_maven.dto.AccountDTO;
import rs.projekatOSA2019_maven.entity.JwtAuthenticationToken;
import rs.projekatOSA2019_maven.entity.JwtUserDetails;

@Component
public class JwtAuthenticationProvider extends AbstractUserDetailsAuthenticationProvider {

	@Autowired
	public JwtValidator validator;
	

	@Override
	public boolean supports(Class<?> aClass) {
		// TODO Auto-generated method stub
		return (JwtAuthenticationToken.class.isAssignableFrom(aClass));
	}

	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails,
			UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected UserDetails retrieveUser(String username, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {
		JwtAuthenticationToken jwtToken=(JwtAuthenticationToken)authentication;
		String token=jwtToken.getToken();
		
		 AccountDTO jwtUser = validator.validate(token);

	        if (jwtUser == null) {
	            throw new RuntimeException("JWT Token is incorrect");
	        }

//	        List<GrantedAuthority> grantedAuthorities = AuthorityUtils
//	                .commaSeparatedStringToAuthorityList(jwtUser.getUsername());
	        return new JwtUserDetails(jwtUser.getUsername(), token,
	                jwtUser.getId());
//	grantedAuthorities);
	}

}
