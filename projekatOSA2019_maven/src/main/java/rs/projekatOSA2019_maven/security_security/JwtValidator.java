package rs.projekatOSA2019_maven.security_security;

import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwt;
import io.jsonwebtoken.Jwts;
import rs.projekatOSA2019_maven.dto.AccountDTO;

@Component
public class JwtValidator {

	 private String secret = "youtube";

	    public AccountDTO validate(String token) {

	        AccountDTO jwtUser = null;
	        try {
	            Claims body = Jwts.parser()
	                    .setSigningKey(secret)
	                    .parseClaimsJws(token)
	                    .getBody();

	            jwtUser = new AccountDTO();

	            jwtUser.setUsername(body.getSubject());
	            jwtUser.setId(Integer.parseInt((String) body.get("userId")));
//	            jwtUser.setRole((String) body.get("role"));
	        }
	        catch (Exception e) {
	            System.out.println(e);
	        }

	        return jwtUser;
	}

}
